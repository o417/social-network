import errorHandler from './error-handler';
import responseBuilder from './response-builder';

const middlewares = {
    errorHandler,
    responseBuilder,
}

export default middlewares;
