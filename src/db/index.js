// Fake db only for examples

import user from "../api/controllers/user"

const users = {
    ohad: {
        usename: 'ohad',
        password: '123456',
        groups: [1]
    },
    tal: {
        usename: 'tal',
        password: '123456',
        groups: [1,2]
    },
    lior: {
        usename: 'lior',
        password: '123456',
        groups: [2]
    }
}

const groups = {
    1 : {
        users: [users.ohad, users.tal]
    },
    2 : {
        users: [users.tal, users.lior]
    }
}

export default {
    users,
    groups
}