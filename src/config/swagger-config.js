import yaml from 'js-yaml';
import fs from 'fs';

function loadConf() {
    try {
        return yaml.load(fs.readFileSync('config/swagger.yaml'));
    } catch(e){
        console.error('Failed loading Swagger configuration file', e);
        throw e;
    }
}

export default loadConf;