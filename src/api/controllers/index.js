import user from './user';
import group from './group';

const controllers = {
    user,
    group
};

export default controllers;