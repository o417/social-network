import {Router} from 'express';
import controllers from '../controllers';

const createUserRouter = () => {
    const router = Router({mergeParams: true});
    router
        .get('/', controllers.user.getAllUsers)
    return router;
}

export default createUserRouter;

