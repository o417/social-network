import getAllGroups from './get-all-groups';
import getGroupById from './get-group-by-id';

const group = {
    getAllGroups,
    getGroupById,
}

export default group;
