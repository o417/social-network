import services from '../../services';

const user = {
    getAllUsers:async (req, res, next) => 
    {
        try{
            const users = services.user.getAllUsers();
            res.json(
                {
                    ...users
                }
            );
        } catch(err){
            next(err);
        }
    },
}
 
export default user;
