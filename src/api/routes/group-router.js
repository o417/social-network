import {Router} from 'express';
import controllers from '../controllers';

const createGroupRouter = () => {
    const router = Router({mergeParams: true});
    router
        .get('/', controllers.group.getAllGroups)
        .get('/id/:id', controllers.group.getGroupById);
    return router;
}

export default createGroupRouter;

