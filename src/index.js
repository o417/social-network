import express from 'express';
import {urlencoded, json } from 'body-parser';
import router from './api/routes/main-router';
import logger from './loaders';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './config/swagger-config';
import middlewares from './api/middlewares';

const app = express();
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument()));
app.use('/api',
    urlencoded({extended: true}),
    json(),
    router(),
    middlewares.responseBuilder,
    middlewares.errorHandler
    );

app.listen(8080, () => logger.info(`backend is listening on port 8080`))