class NotExistsError extends Error {
    constructor({message}) {
        super();
        this.name = 'NotExistsError';
        this.message = message;
        this.statusCode = 404;
    }
}

export default NotExistsError;