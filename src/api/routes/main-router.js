import {Router} from 'express';
import userRouter from './user-router';
import groupRouter from './group-router'

const createMainRouter = () => {
    const router = Router({mergeParams: true});
    return router
        .use('/user', userRouter())
        .use('/group', groupRouter());
}

export default createMainRouter;

