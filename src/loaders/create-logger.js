import winston, { transports } from 'winston';

const logDataFormat = winston.format.printf(({level, message, timestamp}) => {
    return `${timestamp} ${level} : ${message}`;
});

const logFormat = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    logDataFormat,
);

export const createLogger = (logsFileName, fileLoggerLevel, consoleLoggerLevel) => 
winston.createLogger({
    transports: [
        new winston.transports.File({
            filename: logsFileName,
            level: fileLoggerLevel,
            timestamp: true,
            json: true,
            format: logFormat
        }),
        new winston.transports.Console({
            level: consoleLoggerLevel,
            timestamp: true,
            json: true,
            format: winston.format.combine(winston.format.colorize(), logFormat)
        })
    ]
});