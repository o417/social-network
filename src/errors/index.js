import NotExistsError from './not-exists-error';

const errors = {
    NotExistsError,
}

export default errors;