/**
 * This middleware function overrides the default express response builder.
 * Every request to undeclreard route reach this fucntion
 */

export default (req, res, next) => {
    res
        .status(404)
        .json({
            status: 'error',
            message: `The route \'${req.originalUrl}\' not found`
        })
}