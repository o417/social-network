import logger from '../../loaders';

/**
 * This middleware fucntion overrides the default express error handler.
 * Every call to next(err) will reach this function
 */

export default (err, req, res, next) => {
    logger.error(`${err.name}: err ${err.message}`);
    res
        .status(err.statusCode ? err.statusCode : 500)
        .json({
            status: 'error',
            message: err.message
        });

};