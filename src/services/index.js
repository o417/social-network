import user from './user';
import group from './group';

const services = {
    user,
    group,
}

export default services;
