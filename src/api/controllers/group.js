import services from '../../services';

const group = {
    getAllGroups: async (req, res, next) => {
        try{
            const groups = services.group.getAllGroups();
            res.json(
                { ...groups }
            );
        } catch(err){
            next(err);
        }
    },
    getGroupById: async (req, res, next) => {
        try{
            const {id} =  req.params;
            const group = services.group.getGroupById(id);
            res.json(
                {...group}
            );
        } catch(err){
            next(err);
        }
    }
}
 
export default group;
