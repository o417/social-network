import db from '../../db'

const getAllGroups = () => {
    return db.groups;
}

export default getAllGroups;
