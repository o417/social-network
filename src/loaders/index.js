import { createLogger } from './create-logger';

const logger = createLogger('./logs/logs.log', 'silly', 'info');

export default logger;
