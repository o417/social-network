import db from '../../db'
import errors from '../../errors';

const getGroupById = (id)=> {
        if(!Object.keys(db.groups).includes(id)){
            throw new errors.NotExistsError({
                message: `The group id \'${id}\' not found`
            })
        }
        return db.groups[id];
    }

 
export default getGroupById;